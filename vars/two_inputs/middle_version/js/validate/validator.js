$(document).ready(function () {
    var forms = $('div.form')

    Inputmask.extendDefaults({
        placeholder: ""
    });
    
    function changeNameId(i){
        return function(){
            $("#send").attr('id', 'send-' + i);
            $("#name").attr('id', 'name-' + i);
            $("#phone").attr('id', 'phone-' + i);
            $('#error-name').attr('id', 'error-name-' + i).html('Введите корректно имя');
            $('#error-phone').attr('id', 'error-phone-' + i).html('Введите корректно телефон');
            $('#success').attr('id', 'success-' + i);
            $("#phone-" + i).inputmask({"mask": "+7 999 999 99 99"});

        }
    }

    function callSubmit(i){
        return function(e){
            e.preventDefault();
            var name = $("#name-" + i);
            var phone = $("#phone-" + i);
            var errorName = $('#error-name-' + i);
            var errorPhone = $('#error-phone-' + i);
            var success = $('#success-' + i);
            var formHide = $("#send-" + i);
            var validName = '^[a-zA-Zа-яА-Я ]{2,35}$';

            if ( $(phone).val().length < 16 || !$(name).val().match(validName) ) {
                if ( !$(name).val().match(validName) ) {
                    $(errorName).css('display', 'block');
                    $(name).css('border', '2px solid red');
                    e.preventDefault();
                } else {
                    $(errorName).css('display', 'none');
                    $(name).css('border', 'none');
                    e.preventDefault();
                }
                if ( $(phone).val().length < 16 ) {
                    $(errorPhone).css('display', 'block');
                    $(phone).css('border', '2px solid red');
                    e.preventDefault();
                } else {
                    $(errorPhone).css('display', 'none');
                    $(phone).css('border', 'none');
                    e.preventDefault();
                }
            } else {
                $(errorName).css('display', 'none');
                $(name).css('border', 'none');
                $(errorPhone).css('display', 'none');
                $(phone).css('border', 'none');
                var showSuccess = function(){
                    $(success).fadeIn(500)
                };
                setTimeout(showSuccess, 500);
                $(formHide).fadeOut(500)
                e.preventDefault();
            }
        }
    }
      
    for(var i = 0; i <= forms.length; i++) {
          $(forms[i]).ready( changeNameId(i) );
          $(forms[i]).submit( callSubmit(i) );
    }

});






            