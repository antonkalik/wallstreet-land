$(document).ready(function () {
    // Slider
    var mySwiper = new Swiper('#swiper', {
        loop: true,
        nextButton: '.next',
        prevButton: '.prev',
        slidesPerView: 1,
        pagination: '.pag',
        paginationClickable: true,
        spaceBetween: 40,
        onTransitionStart: function () {
            var currentTitle = $(".swiper-slide-active .rec_slide img").attr('title')
            $('.des').text(currentTitle).hide()
        },
        onTransitionEnd: function (mySwiper) {
            var currentTitle = $(".swiper-slide-active .rec_slide img").attr('title')
            $('.des').text(currentTitle).fadeIn("slow");
        }
      });
});