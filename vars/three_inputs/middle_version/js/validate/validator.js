$(document).ready(function () {
    var forms = $('div.form')

    Inputmask.extendDefaults({
        placeholder: ""
    });
    
    function changeNameId(i){
        return function(){
            $("#send").attr('id', 'send-' + i);
            $("#name").attr('id', 'name-' + i);
            $("#email").attr('id', 'email-' + i);
            $("#phone").attr('id', 'phone-' + i);
            $('#error-name').attr('id', 'error-name-' + i).html('Введите корректно имя');
            $('#error-mail').attr('id', 'error-mail-' + i).html('Введите корректно почту');
            $('#error-phone').attr('id', 'error-phone-' + i).html('Введите корректно телефон');
            $('#success').attr('id', 'success-' + i);
            $("#phone-" + i).inputmask({"mask": "+7 999 999 99 99"});
            $('#email-' + i).inputmask({
                greedy: false,
                definitions: {
                '*': {
                    validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                    casing: "lower"
                }
                }
            });
        }
    }

    function callSubmit(i){
        return function(e){
            e.preventDefault();
            var name = $("#name-" + i);
            var email = $("#email-" + i);
            var phone = $("#phone-" + i);
            var errorName = $('#error-name-' + i);
            var errorMail = $('#error-mail-' + i);
            var errorPhone = $('#error-phone-' + i);
            var success = $('#success-' + i);
            var formHide = $("#send-" + i);
            var sEmail = $('#email-' + i).val();
            var validName = '^[a-zA-Zа-яА-Я ]{2,35}$';
            console.log(formHide)

            if ( !validateEmail(sEmail) || $(phone).val().length < 16 || !$(name).val().match(validName) ) {
                if ( !$(name).val().match(validName) ) {
                    $(errorName).css('display', 'block');
                    $(name).css('border', '2px solid red');
                    e.preventDefault();
                } else {
                    $(errorName).css('display', 'none');
                    $(name).css('border', 'none');
                    e.preventDefault();
                }
                if (!validateEmail(sEmail)) {
                    $(errorMail).css('display', 'block');
                    $(email).css('border', '2px solid red');
                    e.preventDefault();
                } else {
                    $(errorMail).css('display', 'none');
                    $(email).css('border', 'none');
                    e.preventDefault();
                }
                if ( $(phone).val().length < 16 ) {
                    $(errorPhone).css('display', 'block');
                    $(phone).css('border', '2px solid red');
                    e.preventDefault();
                } else {
                    $(errorPhone).css('display', 'none');
                    $(phone).css('border', 'none');
                    e.preventDefault();
                }
            } else {
                $(errorName).css('display', 'none');
                $(name).css('border', 'none');
                $(errorPhone).css('display', 'none');
                $(phone).css('border', 'none');
                $(errorMail).css('display', 'none');
                $(email).css('border', 'none');
                var showSuccess = function(){
                    $(success).fadeIn(500)
                };
                setTimeout(showSuccess, 500);
                $(formHide).fadeOut(500)
                e.preventDefault();
            }

            function validateEmail(sEmail) {
                var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if (filter.test(sEmail)) {
                return true;
            }
                else {
                    return false;
            }
        }
            
        }
    }
      
    for(var i = 0; i <= forms.length; i++) {
          $(forms[i]).ready( changeNameId(i) );
          $(forms[i]).submit( callSubmit(i) );
    }

});






            