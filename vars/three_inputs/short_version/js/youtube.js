$(document).ready(function() {
      $(".youtube").each(function() {
        $(this).css('background-image', 'url("res/images/video.png")');
        $(document).delegate('#' + this.id, 'click', function() {
          var iframe_url = "http://www.youtube.com/embed/" + this.id + "?autoplay=1&autohide=1";
          if ($(this).data('params')) iframe_url += '&' + $(this).data('params');
          var iframe = $('<iframe/>', {'allowfullscreen':'allowfullscreen', 'frameborder': '0', 'src': iframe_url})
          $(this).append(iframe);
        });
      });
});