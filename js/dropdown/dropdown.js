;(function($, window, document, undefined) {
  'use strict';
    var $html = $('html');
    var $inp = $('c-dropdown__item');
    $html.on('click.ui.dropdown', '.js-dropdown', function(e) {
      $(this).toggleClass('is-open');
    });

    $html.on('click.ui.dropdown', '.js-dropdown [data-dropdown-value]', function(e) {
      var $item = $(this);
      var $dropdown = $item.parents('.js-dropdown');
      $dropdown.find('.js-dropdown__input').val($item.data('dropdown-value'));
      $dropdown.find('.js-dropdown__current').text($item.text());
      $( $item ).click(function( event ) {
        var target = $( event.target );
        if ( target.is( ":checked" ) ) { 
          $('#next').css('color', 'rgba(255, 255, 255, 1)');
          $('.c-button').css('border', 'none')
        }
        if ( $('.js-dropdown').hasClass('last-q') ) { $('#next').text('Посмотреть результаты').css('color', '#2FC050') }
      });
      $('#next').click(function( event ) {
        var target = $( event.target );
        target.css('color', 'rgba(255, 255, 255, 0.2)');
      });
    });
    
    $html.on('click.ui.dropdown', function(e) {
      $( ".c-dropdown__item" ).click(function() {
        $('.js-dropdown').removeClass('is-open');
      });
      var $target = $(e.target);
      if (!$target.parents().hasClass('js-dropdown')) {
        $('.js-dropdown').removeClass('is-open');
      }
    });
    
  })(jQuery, window, document);