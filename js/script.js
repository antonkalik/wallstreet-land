// JavaScript Document
$(document).ready(function(){
  "use strict";

  var questions = [{
    question: 'My name’s James Bond. What’s your name? <div class="c-dropdown js-dropdown"><input type="hidden" name="Framework" id="Framework" class="js-dropdown__input"><span class="c-button c-button--dropdown js-dropdown__current"><font style="white-space:nowrap;color:rgba(255, 255, 255, 0)" face="arial">_____________</font></span><ul class="c-dropdown__list"><li data-dropdown-value="1"><input type="radio" id="choise1" name="answer" value="1"><label for="choise1" class="c-dropdown__item">My name’s Alice.</label></li><li data-dropdown-value="2"><input type="radio" id="choise2" name="answer" value="2"><label for="choise2" class="c-dropdown__item">My name Alice.</label></li><li data-dropdown-value="3"><input type="radio" id="choise3" name="answer" value="3"><label for="choise3" class="c-dropdown__item">I Alice.</label></li></ul></div>',
    choices: ['My name’s Alice.', 'My name Alice.', 'I Alice.'],
    correctAnswer: 1
  }, {
    question: '<div class="c-dropdown js-dropdown"><input type="hidden" name="Framework" id="Framework" class="js-dropdown__input"><span class="c-button c-button--dropdown js-dropdown__current"><font style="white-space:nowrap;color:rgba(255, 255, 255, 0)" face="arial">_____</font></span><ul class="c-dropdown__list"><li data-dropdown-value="1"><input type="radio" id="choise1" name="answer" value="1"><label for="choise1" class="c-dropdown__item">When</label></li><li data-dropdown-value="2"><input type="radio" id="choise2" name="answer" value="2"><label for="choise2" class="c-dropdown__item">What</label></li><li data-dropdown-value="3"><input type="radio" id="choise3" name="answer" value="3"><label for="choise3" class="c-dropdown__item">Where</label></li></ul></div> are you from? I’m from Moscow. I came to London with Wall Street.',
    choices: ["When", "What", "Where"],
    correctAnswer: 3
  }, {
    question: 'Learning English will help me to improve <div class="c-dropdown js-dropdown"><input type="hidden" name="Framework" id="Framework" class="js-dropdown__input"><span class="c-button c-button--dropdown js-dropdown__current"><font style="white-space:nowrap;color:rgba(255, 255, 255, 0)" face="arial">________</font></span><ul class="c-dropdown__list"><li data-dropdown-value="1"><input type="radio" id="choise1" name="answer" value="1"><label for="choise1" class="c-dropdown__item">my career</label></li><li data-dropdown-value="2"><input type="radio" id="choise2" name="answer" value="2"><label for="choise2" class="c-dropdown__item">my carrier</label></li><li data-dropdown-value="3"><input type="radio" id="choise3" name="answer" value="3"><label for="choise3" class="c-dropdown__item">my caring</label></li></ul></div> opportunities.',
    choices: ["my career", "my carrier", "my caring"],
    correctAnswer: 1
  }, {
    question: 'Taking classes from different native teachers in a fully English speaking environment <div class="c-dropdown js-dropdown"><input type="hidden" name="Framework" id="Framework" class="js-dropdown__input"><span class="c-button c-button--dropdown js-dropdown__current"><font style="white-space:nowrap;color:rgba(255, 255, 255, 0)" face="arial">____________</font></span><ul class="c-dropdown__list"><li data-dropdown-value="1"><input type="radio" id="choise1" name="answer" value="1"><label for="choise1" class="c-dropdown__item">will help me to</label></li><li data-dropdown-value="2"><input type="radio" id="choise2" name="answer" value="2"><label for="choise2" class="c-dropdown__item">will me</label></li><li data-dropdown-value="3"><input type="radio" id="choise3" name="answer" value="3"><label for="choise3" class="c-dropdown__item">not</label></li></ul></div> learn the language faster.',
    choices: ["will help me to", "will me", "not"],
    correctAnswer: 1
  }, {
    question: 'What do you want <div class="c-dropdown js-dropdown"><input type="hidden" name="Framework" id="Framework" class="js-dropdown__input"><span class="c-button c-button--dropdown js-dropdown__current"><font style="white-space:nowrap;color:rgba(255, 255, 255, 0)" face="arial">_______</font></span><ul class="c-dropdown__list"><li data-dropdown-value="1"><input type="radio" id="choise1" name="answer" value="1"><label for="choise1" class="c-dropdown__item">me to do</label></li><li data-dropdown-value="2"><input type="radio" id="choise2" name="answer" value="2"><label for="choise2" class="c-dropdown__item">me do</label></li><li data-dropdown-value="3"><input type="radio" id="choise3" name="answer" value="3"><label for="choise3" class="c-dropdown__item">I do</label></li></ul></div> with all these books?',
    choices: ["me to do", "me do", "I do"],
    correctAnswer: 1
  }, {
    question: 'Wall Street guarantees that you’ll learn English. 97.2% of our students <div class="c-dropdown js-dropdown"><input type="hidden" name="Framework" id="Framework" class="js-dropdown__input"><span class="c-button c-button--dropdown js-dropdown__current"><font style="white-space:nowrap;color:rgba(255, 255, 255, 0)" face="arial">________</font></span><ul class="c-dropdown__list"><li data-dropdown-value="1"><input type="radio" id="choise1" name="answer" value="1"><label for="choise1" class="c-dropdown__item">relieved</label></li><li data-dropdown-value="2"><input type="radio" id="choise2" name="answer" value="2"><label for="choise2" class="c-dropdown__item">received</label></li><li data-dropdown-value="3"><input type="radio" id="choise3" name="answer" value="3"><label for="choise3" class="c-dropdown__item">retrieved</label></li></ul></div> their certificates.',
    choices: ["relieved", "received", "retrieved"],
    correctAnswer: 2
  }, {
    question: 'In the past 42 years, 3 million people in 28 countries <div class="c-dropdown js-dropdown last-q"><input type="hidden" name="Framework" id="Framework" class="js-dropdown__input"><span class="c-button c-button--dropdown js-dropdown__current"><font style="white-space:nowrap;color:rgba(255, 255, 255, 0)" face="arial">______</font></span><ul class="c-dropdown__list"><li data-dropdown-value="1"><input type="radio" id="choise1" name="answer" value="1"><label for="choise1" class="c-dropdown__item">learned</label></li><li data-dropdown-value="2"><input type="radio" id="choise2" name="answer" value="2"><label for="choise2" class="c-dropdown__item">learning</label></li><li data-dropdown-value="3"><input type="radio" id="choise3" name="answer" value="3"><label for="choise3" class="c-dropdown__item">learnd</label></li></ul></div> English at Wall Street.',
    choices: ["learned", "learning", "learnd"],
    correctAnswer: 1
  }];

  $.each(questions, function(index, value) {
    $('<div class="dot"/>').appendTo('.dots');
  });

  var questionCounter = 0; //Tracks question number
  var selections = []; //Array containing user choices
  var quiz = $('.content'); //Quiz div object
  var quiztitle = $('.quiztitle');

  function dotColorChange() {
    var dot = $('.dot')
    var elementsHavingTestClass = dot.filter('.backred');
    $(dot[questionCounter]).addClass( "backred" );
  }
  
  // Display initial question
  displayNext();
  dotColorChange()


  // Click handler for the 'next' button
  $('#next').on('click', function (e) {
    e.preventDefault();
    
    // Suspend click listener during fade animation
    if(quiz.is(':animated')) {        
      return false;
    }
    choose();

    // If no user selection, progress is stopped
    if (isNaN(selections[questionCounter])) {
      $('#warning').text('');
      $('.c-button').css('border', '2px solid red')
    } else {
      var dot = $('.dot')
      var elementsHavingTestClass = dot.filter('.backred');
      questionCounter++;
      $(dot[questionCounter]).addClass( "backred" );
      displayNext();
	  $('#warning').text('');
    }
  });
  
  // Creates and returns the div that contains the questions and the answer selections
  function createQuestionElement(index) {
    var qElement = $('<div>', {
      id: 'question'
    });
    
    var question = $('<p>').append(questions[index].question);
    qElement.append(question);
    
    var radioButtons = createRadios(index);
    qElement.append(radioButtons);

    // this is new
    var warningText = $('<p id="warning">');
    qElement.append(warningText);
    
    return qElement;

  }
  
  // Creates a list of the answer choices as radio inputs
  function createRadios(index) {}
  
  // Reads the user selection and pushes the value to an array
  function choose() {
    selections[questionCounter] = +$('input[name="answer"]:checked').val();
  }
  
  // Displays next requested element
  function displayNext() {
    quiz.fadeOut(function() {
      $('#question').remove();
      
      if(questionCounter < questions.length){
        var nextQuestion = createQuestionElement(questionCounter);
        quiz.append(nextQuestion).fadeIn();
        if (!(isNaN(selections[questionCounter]))) {
          $('input[value='+selections[questionCounter]+']').prop('checked', true);
        }
        
        // Controls display of 'prev' button
        if(questionCounter === 0){
          $('#next').show();
        }
       } else {
        var scoreElem = displayScore();
        var disForm = displayForm()
        quiztitle.append(scoreElem).fadeIn();
        quiz.append(disForm).fadeIn();
        $('#next').hide();
      }
    });
  }
  
  // Computes score and returns a paragraph element to be displayed

  function displayScore() {
    var score = $('.result')
    var count = $('.count')
    var numCorrect = 0;
    for (var i = 0; i < selections.length; i++) {
      if (selections[i] === questions[i].correctAnswer) {
        numCorrect++;
      }
    }
	// Calculate score and display relevant message
	var percentage = numCorrect / questions.length;
	if (percentage >= 0.9){
      score.text('Отличный результат, ваш уровень выше среднего! Продолжайте совершенствовать свои знания! Оставьте заявку на обучение и заговорите на английском языке, как на родном!');
      count.text(numCorrect + ' из ' + questions.length + ' ответов верные.')
     
	}
	
	else if (percentage >= 0.7){
      score.text('Это неплохой результат, но мы знаем, как его улучшить! Оставьте заявку на обучение и забудьте про переводчиков и словари!');
      count.text(numCorrect + ' из ' + questions.length + ' ответов верные.')
	}
	else {
      score.text('Недовольны своим результатом? Мы знаем, как его улучшить! Оставьте заявку на обучение в Wall Street English и начните говорить на английском с первого занятия!');
      count.text(numCorrect + ' из ' + questions.length + ' ответов верные.')
	}
    return score;
  }
  function displayForm() {
    $('.show-form').fadeIn()
    $('.controls').hide()
    $('.count').show()
    $('.count').css('display', 'block')
    $('.respond').show()
    $('.respond').css('display', 'block')
  }
  
});

// Remove Sticky

if ($(window).width() < 900) {
  $('.left-side').removeClass('sticky');
}

// Slider Show Discription

